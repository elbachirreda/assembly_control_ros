#pragma once

#include <ros/ros.h>
#include <string>

template <  typename InputMsgT_supply_conveyor,
            typename OutputMsgT_supply_conveyor,
	        typename InputMsgT_camera,
	        typename OutputMsgT_camera,
	        typename InputMsgT_robot,
            typename OutputMsgT_robot,
	        typename InputMsgT_controleur,
            typename OutputMsgT_controleur,
	        typename InputMsgT_evacuation_conveyor,
            typename OutputMsgT_evacuation_conveyor,
            typename InputMsgT_assembly_station,
            typename OutputMsgT_assembly_station>

class Mon_controleur {
public:
    Mon_controleur(ros::NodeHandle node, const std::string& topic)
        : node_(node) {


        output_publisher_supply_conveyor = node.advertise<InputMsgT_supply_conveyor>("supply_conveyor/input", 10);
        output_publisher_camera = node.advertise<InputMsgT_camera>("camera/input", 10);
        output_publisher_robot = node.advertise<InputMsgT_robot>("robot/input", 10);
        output_publisher_controleur = node.advertise<InputMsgT_controleur>("controleur/input", 10);
        output_publisher_evacuation_conveyor = node.advertise<InputMsgT_evacuation_conveyor>("evacuation_conveyor/input", 10);
        output_publisher_assembly_station = node.advertise<InputMsgT_assembly_station>("assembly_station/input", 10);


        input_subscriber_supply_conveyor = node.subscribe("supply_conveyor/output", 10, &Mon_controleur::input_supply_conveyor_Callback, this);
        input_subscriber_camera = node.subscribe("camera/output", 10, &Mon_controleur::input_camera_Callback, this);	    
        input_subscriber_robot = node.subscribe("robot/output", 10, &Mon_controleur::input_robot_Callback, this);	    
        input_subscriber_controleur = node.subscribe("controleur/output", 10, &Mon_controleur::input_controleur_Callback, this);	    
        input_subscriber_evacuation_conveyor = node.subscribe("evacuation_conveyor/output", 10, &Mon_controleur::input_evacuation_conveyor_Callback, this);	    
        input_subscriber_assembly_station = node.subscribe("assembly_station/output", 10, &Mon_controleur::input_assembly_station_Callback, this);



    }

    virtual void process() = 0;

protected:

    void sendOuputs_supply_conveyor(InputMsgT_supply_conveyor output_message) {
        output_publisher_supply_conveyor.publish(output_message);
    }
    void sendOuputs_camera(InputMsgT_camera output_message) {
    output_publisher_camera.publish(output_message);
    }
    void sendOuputs_robot(InputMsgT_robot output_message) {
    output_publisher_robot.publish(output_message);
    }

    void sendOuputs_evacuation_conveyor(InputMsgT_evacuation_conveyor output_message) {
    output_publisher_evacuation_conveyor.publish(output_message);
    }
    void sendOuputs_assembly_station(InputMsgT_assembly_station output_message) {
    output_publisher_assembly_station.publish(output_message);
    }


    OutputMsgT_supply_conveyor& getInputs_supply_conveyor() {
        return input_message_supply_conveyor;
    }
    OutputMsgT_camera& getInputs_camera() {
        return input_message_camera;
    }
    OutputMsgT_robot& getInputs_robot() {
        return input_message_robot;
    }
    OutputMsgT_controleur& getInputs_controleur() {
        return input_message_controleur;
    }
    OutputMsgT_evacuation_conveyor& getInputs_evacuation_conveyor() {
        return input_message_evacuation_conveyor;
    }
    OutputMsgT_assembly_station& getInputs_assembly_station() {
        return input_message_assembly_station;
    }

private:

    void input_supply_conveyor_Callback(const typename OutputMsgT_supply_conveyor::ConstPtr& message) {
        input_message_supply_conveyor = *message;
    }
    void input_camera_Callback(const typename OutputMsgT_camera::ConstPtr& message) {
        input_message_camera = *message;
    }
    void input_robot_Callback(const typename OutputMsgT_robot::ConstPtr& message) {
        input_message_robot = *message;
    }
    void input_controleur_Callback(const typename OutputMsgT_controleur::ConstPtr& message) {
        input_message_controleur = *message;
    }
    void input_evacuation_conveyor_Callback(const typename OutputMsgT_evacuation_conveyor::ConstPtr& message) {
        input_message_evacuation_conveyor = *message;
    }
    void input_assembly_station_Callback(const typename OutputMsgT_assembly_station::ConstPtr& message) {
        input_message_assembly_station = *message;
    }

 
    ros::NodeHandle node_;

    ros::Publisher output_publisher_supply_conveyor;
    ros::Subscriber input_subscriber_supply_conveyor;

    ros::Publisher output_publisher_camera;
    ros::Subscriber input_subscriber_camera;

    ros::Publisher output_publisher_robot;
    ros::Subscriber input_subscriber_robot;

    ros::Publisher output_publisher_controleur;
    ros::Subscriber input_subscriber_controleur;

    ros::Publisher output_publisher_evacuation_conveyor;
    ros::Subscriber input_subscriber_evacuation_conveyor;

    ros::Publisher output_publisher_assembly_station;
    ros::Subscriber input_subscriber_assembly_station;

    
    OutputMsgT_supply_conveyor input_message_supply_conveyor;
    OutputMsgT_camera input_message_camera;
    OutputMsgT_robot input_message_robot;
    OutputMsgT_controleur input_message_controleur;
    OutputMsgT_evacuation_conveyor input_message_evacuation_conveyor;
    OutputMsgT_assembly_station input_message_assembly_station;


};


