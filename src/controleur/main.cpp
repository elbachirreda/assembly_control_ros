#include <ros/ros.h>

#include <assembly_control_ros/controleur_input.h>
#include <assembly_control_ros/controleur_output.h>
#include <assembly_control_ros/supply_conveyor_input.h>
#include <assembly_control_ros/supply_conveyor_output.h>
#include <assembly_control_ros/camera_input.h>
#include <assembly_control_ros/camera_output.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>

#include <common/Ma_Machine_Ctrl.hpp> //TO_DO

class Controleur
    : public Mon_controleur<assembly_control_ros::supply_conveyor_input,
                            assembly_control_ros::supply_conveyor_output,
	                        assembly_control_ros::camera_input,
                            assembly_control_ros::camera_output,
			                assembly_control_ros::robot_input,
                            assembly_control_ros::robot_output,
                            assembly_control_ros::controleur_input,
                            assembly_control_ros::controleur_output,
			                assembly_control_ros::evacuation_conveyor_input,
                            assembly_control_ros::evacuation_conveyor_output,
                            assembly_control_ros::assembly_station_input,
                            assembly_control_ros::assembly_station_output> {
public:
    Controleur(ros::NodeHandle node)
        : Mon_controleur(node, "controleur"), state_(State::Control_beginning) {
    }

    virtual void process() override {

                            assembly_control_ros::controleur_output outputs;
                            assembly_control_ros::controleur_input inputs;
                            assembly_control_ros::supply_conveyor_input scoutputs;      //declarer les inputs de SC comme outputs pour notre programme
                            assembly_control_ros::camera_input camoutputs;              //declarer les inputs de Camera comme outputs pour notre programme
                            assembly_control_ros::robot_input roboutputs;               //declarer les inputs de Robot comme outputs pour notre programme
                            assembly_control_ros::assembly_station_input asoutputs;     //declarer les inputs de AS comme outputs pour notre programme
                            assembly_control_ros::evacuation_conveyor_input ecoutputs;  //declarer les inputs de EC comme outputs pour notre programme



                            auto& scinputs = getInputs_supply_conveyor();
                            auto& caminputs = getInputs_camera();
                            auto& robinputs = getInputs_robot();
                            auto& continputs = getInputs_controleur();
                            auto& ecinputs = getInputs_evacuation_conveyor();
                            auto& asinputs = getInputs_assembly_station();



 
switch (state_) {
        case State::Control_beginning:

                ros::Duration(5.0).sleep();    //SLEEP To_do       
                roboutputs.in_right = true;
                sendOuputs_robot(roboutputs);
                ROS_INFO("Go right to Supply convyor");
                state_ = State::Go_right;
                
            break;
            
        case State::Go_right:

            if(robinputs.out_right && scinputs.part_available){
                ROS_INFO("in Supply convyor");
                robinputs.out_right = false;
                scinputs.part_available = false;
                
                state_ = State::Suply_convyor1;
            }
            break;

        case State::Suply_convyor1:

            
                ROS_INFO("Camera recognition");
                camoutputs.start_recognition = true;
		        sendOuputs_camera(camoutputs);
                
                state_ = State::Suply_convyor2;
            
            break;         
            
        case State::Suply_convyor2:

            if(caminputs.part_analyzed){
                ROS_INFO("Result Ready of Camera");
                caminputs.part_analyzed = false;
                roboutputs.grasp_in = true;
		        sendOuputs_robot(roboutputs);
                
                state_ = State::Grasp;
            }
            break;

        case State::Grasp:

            if(robinputs.grasp_out){
                ROS_INFO("Robot Graspped");
                robinputs.grasp_out = false;
                scoutputs.restart = true;
		        sendOuputs_supply_conveyor(scoutputs);
                state_ = State::Memory_Decision;
            }
            break;


        case State::Memory_Decision:  
            if(caminputs.part1){  //ToDo
               ROS_INFO("[Camera] Part 1");
               if (want_Part1 == true) {
                    want_Part1 = false;
                    Memoire ++;
                    if(Memoire == 3){
                        want_Part1 = true;
                        want_Part2 = true;
                        want_Part3 = true;
                        Memoire = 0;
                    }
                state_ = State::Ass_Valid;
               ROS_INFO("Part 1 go to ASS");
                }

                else
                {
               ROS_INFO("Part 1 go to EVAC");
                state_ = State::Ec_Valid;
                }
               
            }

            if(caminputs.part2){  //TooooooooooooooooDooooooooooo
               ROS_INFO("[Camera] Part 2");
               if (want_Part2 == true) {
                    want_Part2 = false;
                    Memoire ++;
                    if(Memoire == 3){
                        want_Part1 = true;
                        want_Part2 = true;
                        want_Part3 = true;
                        Memoire = 0;
                    }
                state_ = State::Ass_Valid;
               ROS_INFO("Part 2 go to ASS");
                }

                else
                {
               ROS_INFO("Part 2 go to EVAC");
                state_ = State::Ec_Valid;
                }
               
            }

            if(caminputs.part3){  //TooooooooooooooooDooooooooooo
               ROS_INFO("[Camera] Part 3");
               if (want_Part3 == true) {
                    want_Part3 = false;
                    Memoire ++;
                    if(Memoire == 3){
                        want_Part1 = true;
                        want_Part2 = true;
                        want_Part3 = true;
                        Memoire = 0;
                    }
                state_ = State::Ass_Valid;
               ROS_INFO("Part 3 go to ASS");
                }

                else
                {
               ROS_INFO("Part 3 go to EVAC");
                state_ = State::Ec_Valid;
                } 
            }
        break;


        case State::Ec_Valid:
                ecoutputs.in_tapis_ec = true;
		        sendOuputs_evacuation_conveyor(ecoutputs);
                ROS_INFO("evacuation station run");
                roboutputs.in_right = true;
		        sendOuputs_robot(roboutputs);
                ROS_INFO("Robot Go Right");                
                state_ = State::GO_Right2;
            
            break;

        case State::GO_Right2:

            if(robinputs.out_right){
                ROS_INFO("Robot in evacuation station");
                robinputs.out_right = false;
                roboutputs.release_in = true;
		        sendOuputs_robot(roboutputs);
                state_ = State::arrived;
            }
            break;

        case State::arrived:

            if(robinputs.release_out && ecinputs.out_tapis_ec){
                robinputs.release_out = false;
                ecinputs.out_tapis_ec = false;
                state_ = State::Left1;
            }
            break;

        case State::Left1:

                roboutputs.in_left = true;
		        sendOuputs_robot(roboutputs);
                state_ = State::Left2;
           
            break;

        case State::Left2:

            if(robinputs.out_left){
                robinputs.out_left = false;

                state_ = State::Suply_convyor1;
            }
            break;

        case State::Ass_Valid:
                roboutputs.in_left = true;
		        sendOuputs_robot(roboutputs);
            if(caminputs.part1){
                caminputs.part1 = false;
                state_ = State::part1_ready;
            }
            if(caminputs.part2){
                caminputs.part2 = false;
                state_ = State::part2_ready;
            }
            if(caminputs.part3){
                caminputs.part3 = false;
                state_ = State::part3_ready;
            }
            break;
        
        case State::part1_ready:   //Part1 ASS

            if(robinputs.out_left){
                robinputs.out_left = false;
                roboutputs.in_part1 = true;
		        sendOuputs_robot(roboutputs);
                state_ = State::Analysis_part1;
            }
            break;
        
        case State::Analysis_part1: 

            if(robinputs.out_part1){
                robinputs.out_part1 = false;
                //asoutputs.ROB_AS_PART1 = true;
		Compteur_ASS ++;
		if(Compteur_ASS == 3){ asoutputs.ROB_AS_PART1 = true;
				       asoutputs.ROB_AS_PART2 = true;
				       asoutputs.ROB_AS_PART3 = true;
				       sendOuputs_assembly_station(asoutputs);
				       Compteur_ASS = 0;
				       state_ = State::Control_beginning;
					}
		else {
		//sendOuputs_assembly_station(asoutputs);
                state_ = State::Control_beginning;
			}
            }
            break;
        
        case State::part2_ready:   //Part2 ASS

            if(robinputs.out_left){
                robinputs.out_left = false;
                roboutputs.in_part2 = true;
		        sendOuputs_robot(roboutputs);
                state_ = State::Analysis_part2;
            }
            break;
        
        case State::Analysis_part2: 

            if(robinputs.out_part2){
                robinputs.out_part2 = false;
                Compteur_ASS ++;
		if(Compteur_ASS == 3){ asoutputs.ROB_AS_PART1 = true;
				       asoutputs.ROB_AS_PART2 = true;
				       asoutputs.ROB_AS_PART3 = true;
				       sendOuputs_assembly_station(asoutputs);
				       Compteur_ASS = 0;
				       state_ = State::Control_beginning;
					}
		else {
		//sendOuputs_assembly_station(asoutputs);
                state_ = State::Control_beginning;
			}
            }
            break;
        
        case State::part3_ready:   //Part3 ASS

            if(robinputs.out_left){
                robinputs.out_left = false;
                roboutputs.in_part3 = true;
		        sendOuputs_robot(roboutputs);
                state_ = State::Analysis_part3;
            }
            break;
        
        case State::Analysis_part3: 

            if(robinputs.out_part3){
                robinputs.out_part3 = false;
                Compteur_ASS ++;
		if(Compteur_ASS == 3){ asoutputs.ROB_AS_PART1 = true;
				       asoutputs.ROB_AS_PART2 = true;
				       asoutputs.ROB_AS_PART3 = true;
				       sendOuputs_assembly_station(asoutputs);
				       Compteur_ASS = 0;
				       state_ = State::Control_beginning;
					}
		else {
		//sendOuputs_assembly_station(asoutputs);
                state_ = State::Control_beginning;
			}
            }
            break;
    }
                    }
                            

private:

    bool want_Part1 = true;
    bool want_Part2 = true;
    bool want_Part3 = true;
    int Memoire = 0;
    int Compteur_ASS = 0;

    enum class State {Control_beginning, Go_right, Suply_convyor1, Suply_convyor2, Grasp, Memory_Decision, Ec_Valid, GO_Right2, arrived, Left1, Left2, Ass_Valid, part1_ready, Analysis_part1, part2_ready, Analysis_part2, part3_ready, Analysis_part3};

    State state_;
};



int main(int argc, char* argv[]) {
    ros::init(argc, argv, "controleur");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Controleur controleur(node);

    while (ros::ok()) {
        controleur.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}


