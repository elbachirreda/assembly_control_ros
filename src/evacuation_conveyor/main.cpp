#include <ros/ros.h>
#include <assembly_control_ros/evacuation_conveyor_state.h>
#include <assembly_control_ros/evacuation_conveyor_command.h>
#include <assembly_control_ros/evacuation_conveyor_input.h>
#include <assembly_control_ros/evacuation_conveyor_output.h>
#include <common/machine_controller.hpp>


class Evac_Conveyor
    : public MachineController<assembly_control_ros::evacuation_conveyor_state,                        assembly_control_ros::evacuation_conveyor_input,                               assembly_control_ros::evacuation_conveyor_command,                              assembly_control_ros::evacuation_conveyor_output> {

public:
    Evac_Conveyor(ros::NodeHandle node)
        : MachineController(node, "evacuation_conveyor"), state_(State::EC_ON) {
    }

    virtual void process() override {
        assembly_control_ros::evacuation_conveyor_command commands;
        assembly_control_ros::evacuation_conveyor_output outputs;

        auto& inputs = getInputs();

        switch (state_) {

        case State::EC_ON:
            commands.on = true;
            if (inputs.in_tapis_ec) {
		inputs.in_tapis_ec = false;    
                ROS_INFO("[Evac_Conveyor] start stopping");
                state_ = State::Stopping;
            }
            break;

	case State::Stopping:
            if (getState().stopped){
		ROS_INFO("[Evac_Conveyor] stopped");
                state_ = State::Stopped;
	    }
	    break;   

        case State::Stopped:
                outputs.out_tapis_ec = true;
                sendOuputs(outputs);
                ROS_INFO("[Evac_Conveyor] EC_ON");
                state_ = State::EC_ON;
            break;

        }

        sendCommands(commands);
    }

private:
    enum class State { EC_ON, Stopping, Stopped };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "evacuation_conveyor");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Evac_Conveyor tapis_cv(node);

    while (ros::ok()) {
        tapis_cv.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}


