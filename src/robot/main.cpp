#include <ros/ros.h>
#include <assembly_control_ros/robot_state.h>
#include <assembly_control_ros/robot_command.h>
#include <assembly_control_ros/robot_input.h>
#include <assembly_control_ros/robot_output.h>
#include <common/machine_controller.hpp>

class roboot
    : public MachineController<assembly_control_ros::robot_state,
                               assembly_control_ros::robot_input,
                               assembly_control_ros::robot_command,
                               assembly_control_ros::robot_output> {
public:
    roboot(ros::NodeHandle node)
        : MachineController(node, "robot"), state_(State::Attente_operation) {
    }

    virtual void process() override {
        assembly_control_ros::robot_command commands;
        assembly_control_ros::robot_output outputs;

        auto& inputs = getInputs();


        switch (state_) {



        case State::Attente_operation:
            if (inputs.in_part1) { state_ = State::ROB_ASS_PART1;
                                  inputs.in_part1 = false; }
            if (inputs.in_part2) { state_ = State::ROB_ASS_PART2; 
                                  inputs.in_part2 = false; }
            if (inputs.in_part3) { state_ = State::ROB_ASS_PART3; 
                                  inputs.in_part3 = false; }
            if (inputs.release_in) { state_ = State::ROB_RELEASE; 
                                  inputs.release_in = false; }
            if (inputs.grasp_in) { state_ = State::ROB_GRASP; 
                                  inputs.grasp_in = false; }
            if (inputs.in_right && (~getState().at_evacuation_conveyor)) { inputs.in_right = false;
                                  state_ = State::ROB_RIGHT;}
            if (inputs.in_left && (~getState().at_assembly_station)) { state_ = State::ROB_LEFT; 
                                  inputs.in_left = false;}
        break;



        case State::ROB_ASS_PART1:
          ROS_INFO("[roboot] Go ass Part1");
          commands.assemble_part1 = true;
          sendCommands(commands);

          if(getState().part1_assembled){
                outputs.out_part1 = true;
                sendOuputs(outputs);
                state_ = State::Attente_operation;
                ROS_INFO("[roboot] Back to init state");
            }
        break;


        case State::ROB_ASS_PART2:
          ROS_INFO("[roboot] Go ass Part2");
          commands.assemble_part2 = true;
          sendCommands(commands);

          if(getState().part2_assembled){
                outputs.out_part2 = true;
                sendOuputs(outputs);
                state_ = State::Attente_operation;
                ROS_INFO("[roboot] Back to init state");
            }
        break;


        case State::ROB_ASS_PART3:
          ROS_INFO("[roboot] Go ass Part3");
          commands.assemble_part3 = true;
          sendCommands(commands);

          if(getState().part3_assembled){
                outputs.out_part3 = true;
                sendOuputs(outputs);
                state_ = State::Attente_operation;
                ROS_INFO("[roboot] Back to init state");
            }
        break;
        

        case State::ROB_RELEASE:
          ROS_INFO("[roboot] Release");
          commands.release = true;
          sendCommands(commands);

          if(getState().part_released){
                outputs.release_out = true;
                sendOuputs(outputs);
                state_ = State::Attente_operation;
                ROS_INFO("[roboot] Back to init state");
            }
        break;


        case State::ROB_GRASP:
          ROS_INFO("[roboot] Grasp");
          commands.grasp = true;
          sendCommands(commands);

           if(getState().part_grasped){
                outputs.grasp_out = true;
                sendOuputs(outputs);
                state_ = State::Attente_operation;
                ROS_INFO("[roboot] Back to init state");
            }
        break;


        case State::ROB_RIGHT:
                ROS_INFO("[roboot] Robot Ready to go right");
	              commands.move_right = true;
                sendCommands(commands);
                ros::Duration(1).sleep();
               if(getState().at_evacuation_conveyor && SC_Pose == true) {  
                
	              outputs.out_right = true;
                sendOuputs(outputs);
                EVAC_Pose = true;
                SC_Pose = false;
                state_ = State::Attente_operation;
                ROS_INFO("[roboot] Robot Ready to go EVAC");
                }

               if(getState().at_supply_conveyor && ASS_Pose == true) {
                
                outputs.out_right = true;
                sendOuputs(outputs);
                SC_Pose = true;
                ASS_Pose = false;
                state_ = State::Attente_operation;
                ROS_INFO("[roboot] Robot Ready to go SC");
                }
          break;

        case State::ROB_LEFT:
	              ROS_INFO("[roboot] Robot Ready to go left");
                commands.move_left = true;
                sendCommands(commands);
                ros::Duration(1).sleep();  
               if(getState().at_assembly_station && SC_Pose == true) {
                     
	              outputs.out_left = true;
                sendOuputs(outputs);
                ASS_Pose = true;
                SC_Pose = false;
                state_ = State::Attente_operation;
                ROS_INFO("[roboot] Robot Ready to go ASS");
                }

               if(getState().at_supply_conveyor && EVAC_Pose == true) {
                
                outputs.out_left = true;
                sendOuputs(outputs);
                SC_Pose = true;
                EVAC_Pose = false;
                state_ = State::Attente_operation;
                ROS_INFO("[roboot] Robot Ready to go SC");
                }
          
        break;
 
                        
        }
         sendCommands(commands);
    }

private:
    enum class State { ROB_ASS_PART1, ROB_ASS_PART2, ROB_ASS_PART3,  ROB_GRASP, ROB_RELEASE, ROB_LEFT, ROB_RIGHT, Attente_operation};
        bool ASS_Pose = true;
        bool SC_Pose = false;
        bool EVAC_Pose = false;
    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "robot");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    roboot robot(node);

    while (ros::ok()) {
        robot.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}


