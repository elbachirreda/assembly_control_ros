#include <ros/ros.h>
#include <assembly_control_ros/assembly_station_state.h>
#include <assembly_control_ros/assembly_station_command.h>
#include <assembly_control_ros/assembly_station_input.h>
#include <assembly_control_ros/assembly_station_output.h>
#include <common/machine_controller.hpp>


class Tapis_assemblage
    : public MachineController<assembly_control_ros::assembly_station_state,
assembly_control_ros::assembly_station_input,
assembly_control_ros::assembly_station_command,
assembly_control_ros::assembly_station_output> {

public:
    Tapis_assemblage(ros::NodeHandle node)
        : MachineController(node, "assembly_station"), state_(State::ass_stopped) {
    }

    virtual void process() override {
        assembly_control_ros::assembly_station_command commands;
        assembly_control_ros::assembly_station_output outputs;

        auto& inputs = getInputs();

        switch (state_) {

        case State::ass_stopped:
            if (inputs.ROB_AS_PART1 && inputs.ROB_AS_PART2 && inputs.ROB_AS_PART3) {
		inputs.ROB_AS_PART1 = false;
		inputs.ROB_AS_PART2 = false;
		inputs.ROB_AS_PART3 = false;
                ROS_INFO("[Tapis_assemblage] To As_check");
                state_ = State::AS_CHECK;
            }
            break;

        case State::AS_CHECK:
	    commands.check = true;
        sendCommands(commands);
            if (getState().valid) {
                ROS_INFO("[Tapis_assemblage] Evacuated");
                state_ = State::to_evacuat;
            }
            break;

	     case State::to_evacuat:
            if (getState().evacuated) {
                outputs.out_ass = true;
		        sendOuputs(outputs);
                ROS_INFO("[Tapis_assemblage] finish");
                state_ = State::ass_stopped;
            }
            break;

        }

        sendCommands(commands);
    }

private:
    enum class State { ass_stopped, AS_CHECK, to_evacuat };

    State state_;
};

int main(int argc, char* argv[]) {
    ros::init(argc, argv, "assembly_station");

    ros::NodeHandle node;

    ros::Rate loop_rate(50); // 50 Hz

    Tapis_assemblage ass_station(node);

    while (ros::ok()) {
        ass_station.process();

        ros::spinOnce();

        loop_rate.sleep();
    }
}



